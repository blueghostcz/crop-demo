$(document).ready(function() {
    
    var crop = null;
    var crop_image = $('#crop-image');
    var crop_form = $('#crop-form');

    crop_image.cropper({
        aspectRatio: $(this).data("aspect-ratio"),
        data: {
            x: crop_image.data('x'),
            y: crop_image.data('y'),
            width: crop_image.data('width'),
            height: crop_image.data('height'),
            rotate: 0,
            scaleX: 1,
            scaleY: 1
        },
        crop: function(e) {
            crop = e;
        }
    });
    
    // CROP:
    $('#save-crop').click(function (e) {        
        var image = $("<input>", {type: "hidden", name: "image", value: crop_image.data('id')});
        crop_form.append($(image));
        var x = $("<input>", {type: "hidden", name: "x", value: crop.x});
        crop_form.append($(x));
        var y = $("<input>", {type: "hidden", name: "y", value: crop.y});
        crop_form.append($(y));
        var width = $("<input>", {type: "hidden", name: "width", value: crop.width});
        crop_form.append($(width));
        var height = $("<input>", {type: "hidden", name: "height", value: crop.height});
        crop_form.append($(height));

        crop_form.submit();
    });

    // RESET:
    $('#reset-crop').click(function (e) {
        var image = $("<input>", {type: "hidden", name: "image", value: crop_image.data('id')});
        crop_form.append($(image));
        
        crop_form.submit();
    });
    
});