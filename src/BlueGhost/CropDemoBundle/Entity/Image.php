<?php
namespace BlueGhost\CropDemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Image {

    /**
     * @param string $url
     */
    public function __construct($url = null) {
        $this->url = $url;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int $id
     */
    private $id;

    /**
     * Cesta k obrazku
     *
     * @ORM\Column(type="string")
     * @var double $aspectRatio;
     */
    private $url;

    /**
     * Pomer stran orezu
     *
     * @ORM\Column(type="float", name="aspect_ratio")
     * @var double $aspectRatio;
     */
    private $aspectRatio;

    /**
     * X-souradnice pocatku orezu - absolutni v pixelech
     *
     * @ORM\Column(type="integer", name="start_x")
     * @var int $startX
     */
    private $startX = 0;

    /**
     * Y-souradnice pocatku orezu - absolutni v pixelech
     *
     * @ORM\Column(type="integer", name="start_y")
     * @var int $startY
     */
    private $startY = 0;

    /**
     * X-souradnice konce orezu - absolutni v pixelech
     *
     * @ORM\Column(type="integer", name="end_x", nullable=TRUE)
     * @var int $endX
     */
    private $endX;

    /**
     * Y-souradnice konce orezu - absolutni v pixelech
     *
     * @ORM\Column(type="integer", name="end_y", nullable=TRUE)
     * @var int $endY
     */
    private $endY;

    /**
     * (re)setuje inicialni crop
     *
     * @return bool
     */
    public function resetCrop() {
        if (file_exists($this->getUrl())) {
            list($width, $height) = getimagesize($this->getUrl());

            $this->setStartX(0);
            $this->setStartY(0);

            $crop_height = $width / $this->aspectRatio;
            if ($crop_height <= $height) {
                $this->setEndX($width);
                $this->setEndY($crop_height);
            } else {
                $this->setEndX($height * $this->aspectRatio);
                $this->setEndY($height);
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * Orizne obrazek dle predanych parametru
     * 
     * @param double $x
     * @param double $y
     * @param double $width
     * @param double $height
     */
    public function crop($x, $y, $width, $height) {
        $this->setStartX(round($x));
        $this->setStartY(round($y));
        $this->setEndX(round($x + $width));
        $this->setEndY(round($y + $height));
    }

    public function getWidth() {
        if (isset($this->endX)) {
            return ($this->endX - $this->startX);
        }        
        if (file_exists($this->getUrl())) {
            list($width, $height) = getimagesize($this->getUrl());
            return ($width - $this->startX);
        }else{
            return false;
        }
    }

    public function getHeight() {
        if (isset($this->endY)) {
            return ($this->endY - $this->startY);
        }
        if (file_exists($this->getUrl())) {
            list($width, $height) = getimagesize($this->getUrl());
            return ($height - $this->startY);
        }else{
            return false;
        }
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param float $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    
    /**
     * @return float
     */
    public function getAspectRatio()
    {
        return $this->aspectRatio;
    }

    /**
     * @param float $aspectRatio
     * @return $this
     */
    public function setAspectRatio($aspectRatio)
    {
        $this->aspectRatio = $aspectRatio;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartX()
    {
        return $this->startX;
    }

    /**
     * @param int $startX
     * @return $this
     */
    public function setStartX($startX)
    {
        $this->startX = $startX;
        return $this;
    }

    /**
     * @return int
     */
    public function getStartY()
    {
        return $this->startY;
    }

    /**
     * @param int $startY
     * @return $this
     */
    public function setStartY($startY)
    {
        $this->startY = $startY;
        return $this;
    }

    /**
     * @return int
     */
    public function getEndX()
    {
        return $this->endX;
    }

    /**
     * @param int $endX
     * @return $this
     */
    public function setEndX($endX)
    {
        $this->endX = $endX;
        return $this;
    }

    /**
     * @return int
     */
    public function getEndY()
    {
        return $this->endY;
    }

    /**
     * @param int $endY
     * @return $this
     */
    public function setEndY($endY)
    {
        $this->endY = $endY;
        return $this;
    }

}
