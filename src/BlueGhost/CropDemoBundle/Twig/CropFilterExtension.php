<?php

namespace BlueGhost\CropDemoBundle\Twig;

use BlueGhost\CropDemoBundle\Entity\Image;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;

/**
 * Twig extension k vykreslení ořezaného obrázku z entity Image
 */
class CropFilterExtension extends \Twig_Extension {

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @param CacheManager $cacheManager
     */
    public function __construct(CacheManager $cacheManager) {
        $this->cacheManager = $cacheManager;
    }

    /**
     * @return array
     */
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('crop_filter', array($this, 'cropFilter')),
        );
    }

    /**
     * @param Image $image
     * @param string $width šířka v pixelech
     * @return \Twig_Markup
     */
    public function cropFilter(Image $image, $width) {

        // CROP:
        $startX = $image->getStartX();
        $startY = $image->getStartY();
        $crop_width = $image->getWidth();
        $crop_height = $image->getHeight();

        // RESIZE:
        $height = $width / $image->getAspectRatio();

        // OPTIONS:
        $options = array(
            "persistent_crop_filter" => array(
                "start_x" => $startX,
                "start_y" => $startY,
                "crop_width" => $crop_width,
                "crop_height" => $crop_height,
                "resize_width" => $width,
                "resize_height" => $height
            )
        );

        // ZAVOLAM VLASTNI LIIP_IMAGINE FILTR "persistent_crop":
        return new \Twig_Markup(
            $this->cacheManager->getBrowserPath($image->getUrl(), "persistent_crop", $options),
            'utf8'
        );
    }

    /**
     * @return string
     */
    public function getName() {
        return 'crop_filter_extension';
    }
}