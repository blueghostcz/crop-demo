<?php
namespace BlueGhost\CropDemoBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use BlueGhost\CropDemoBundle\Entity\Image;

class LoadUserData implements FixtureInterface {

    public function load(ObjectManager $manager) {
        $image = new Image("bundles/cropdemo/images/van_gogh_1.jpg");
        $image->setAspectRatio(1);
        $manager->persist($image);

        $image = new Image("bundles/cropdemo/images/van_gogh_2.jpg");
        $image->setAspectRatio(1);
        $manager->persist($image);

        $image = new Image("bundles/cropdemo/images/van_gogh_3.jpg");
        $image->setAspectRatio(1);
        $manager->persist($image);

        $image = new Image("bundles/cropdemo/images/van_gogh_4.jpg");
        $image->setAspectRatio(1);
        $manager->persist($image);

        $manager->flush();
    }
    
}