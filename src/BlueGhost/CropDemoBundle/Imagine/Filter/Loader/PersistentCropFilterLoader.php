<?php

namespace BlueGhost\CropDemoBundle\Imagine\Filter\Loader;

use Imagine\Filter\Basic\Crop;
use Imagine\Filter\Basic\Resize;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;
use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;

/**
 *
 * {@link http://symfony.com/doc/current/bundles/LiipImagineBundle/filters.html#load-your-custom-filters custom filter loader}
 *
 */
class PersistentCropFilterLoader implements LoaderInterface{

    /**
     * Dostane $image a pole $options s údaji o ořezu a zvětšení
     * a vrátí požadovaný obrázek
     */
    public function load (ImageInterface $image, array $options = array()) {

        $cropFilter = new Crop(
            new Point($options["start_x"], $options["start_y"]),
            new Box($options["crop_width"], $options["crop_height"])
        );

        $resizeFilter = new Resize(
            new Box($options["resize_width"], $options["resize_height"])
        );

        // crop, potom resize na velikost, kterou chteji:
        $image = $cropFilter->apply($image);
        $image = $resizeFilter->apply($image);

        return $image;
    }
}