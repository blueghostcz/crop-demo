<?php

namespace BlueGhost\CropDemoBundle\Controller;

use BlueGhost\CropDemoBundle\Entity\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller {
    /**
     * @Route("/", name="index")
     * @Method("GET")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction (Request $request) {
        $images = $this->getDoctrine()->getRepository("CropDemoBundle:Image")->findAll();
        $crop_image = $images[0];

        if ($request->query->has("crop")) {
            $crop_id = $request->query->get("crop");
            $image = $this->getDoctrine()->getRepository("CropDemoBundle:Image")->find($crop_id);
            if ($image) {
                $crop_image = $image;
            }
        }
        
        return $this->render('CropDemoBundle::index.html.twig', array(
            "crop_image" => $crop_image,
            "images" => $images
        ));
    }

    /**
     * @Route("/", name="save")
     * @Method("POST")
     *
     * @param Request $request
     * @return Response
     */
    public function saveAction (Request $request) {
        $manager = $this->getDoctrine()->getManager();
        $images = $manager->getRepository("CropDemoBundle:Image")->findAll();
        
        if ($request->request->has("image") and $request->request->has("action")) {
            $crop_id = $request->request->get("image");
            $image = $this->getDoctrine()->getRepository("CropDemoBundle:Image")->find($crop_id);
            if (!$image) {
                throw new NotFoundHttpException("Resource not found");
            }
            $action = $request->request->get("action");
            if ($action == "save") {
                // ----------------------------------------------
                //  ULOZ OREZ:
                // ---------------------------------------------- 
                if ($request->request->has("x") and
                    $request->request->has("y") and 
                    $request->request->has("width") and
                    $request->request->has("height")) 
                {
                    $image->crop(
                        $request->request->get("x"), 
                        $request->request->get("y"), 
                        $request->request->get("width"), 
                        $request->request->get("height")
                    );
                    $manager->persist($image);
                    $manager->flush();
                }else{
                    throw new BadRequestHttpException("Missing form data");
                }
            }elseif ($action == "reset") {
                // ----------------------------------------------
                //  RESETUJ OREZ:
                // ----------------------------------------------
                $image->resetCrop();
                $manager->persist($image);
                $manager->flush();
            }else{
                throw new BadRequestHttpException("Invalid action");
            }
        }else{
            throw new BadRequestHttpException("Missing form data");
        }
        
        return $this->render('CropDemoBundle::index.html.twig', array(
            "crop_image" => $image,
            "images" => $images
        ));
    }
    
}
