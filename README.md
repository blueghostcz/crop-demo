# Nedestruktivní ořezávání obrázků v Symfony za pomoci LiipImagineBundlu a jQuery Cropperu

## Demo:
* [crop-demo.blueghost.cz/](http://crop-demo.blueghost.cz/)

## Liip Imagine Bundle:
* [GitHub](https://github.com/liip/LiipImagineBundle)
* [Symfony docs](http://symfony.com/doc/current/bundles/LiipImagineBundle/index.html)

## Cropper:
* [GitHub](https://github.com/fengyuanchen/cropper)
* [Website](https://fengyuanchen.github.io/cropper/)

## Jak jsme spojili funkčnost **LiipImagineBundlu** a **Cropperu** pro snadné, nedestruktivní ořezávání obrázků nahraných do **Symfony** aplikace

_(pozn.: Titulek bych zjednodušil, záleží asi dle publikačního média…)_

[Imagine](https://imagine.readthedocs.io/en/latest/index.html) je šikovná _PHP OOP_ knihovna pro manipulaci s obrázky. **LiipImagineBundle** přináší její snadné začlenění a použití v rámci Symfony aplikací (dokumentace [přímo na stránkách Symfony](http://symfony.com/doc/master/bundles/LiipImagineBundle/index.html)).

### Bundle nainstalujete třemi jednoduchými kroky:
_(pozn.: Nevím, jaké je cílové publikum, možná je následující sekce o instalaci a základních příkladech užití zbytečná, pokud má být článek pro ostřílené „symfoňáky“ - ti si to přečtou v dokumentaci nebo na GitHubu bundlu...)_

1. V kořenovém adresáři projektu (pokud máte Composer zaregistrovaný globálně) zavolejte: `composer require liip/imagine-bundle`
2. V souboru app/appKernel.php přidejte ve funkci registerBundles() následující řádek do pole $bundles:
`new Liip\ImagineBundle\LiipImagineBundle(),`
3. V souboru app/config/routing.yml přidejte následující řádky:

        _liip_imagine:
            resource: "@LiipImagineBundle/Resources/config/routing.xml"

### Základní použití:

V souboru konfigurace si nastavíte sady filtrů (filter sets) tak, jak je potřebujete někde v aplikaci použít. Například pro tvorbu náhledu obrázku o velikosti 120*90 px vytvoříte filtr **my_thumb**:

    # app/config/config.yml

    liip_imagine:
        resolvers:
            default:
            web_path: ~

        filter_sets:
            cache: ~
            my_thumb:
                quality: 75
                filters:
                    thumbnail: { size: [120, 90], mode: outbound }
                    
Ten potom použijete přímo v twigové šabloně:

`<img src="{{ '/relative/path/to/image.jpg' | imagine_filter('my_thumb') }}" />`

Při prvním požadavku na tento obrázek bundle aplikuje zadaný filtr na zdrojový obrázek a výsledek **uloží do cache** (`web/media/cache`). Uložený obrázek pak aplikace vrací při všech dalších požadavcích.

___

_(pozn.: tady začíná pokročilý obsah:)_

Kromě zmenšení/zvětšení obrázku můžete za použití předpřipravených filtrů obrázky mj. ořezávat, přidávat vodoznaky, otáčet či jim změnit pozadí. Velmi často vám ale nebude takto staticky definovaný filtr dostačovat. 

Představte si, že dovolíte uživateli vaší aplikace nahrávat vlastní obrázky, které mu rovnou v rámci aplikace umožníte ořezat v nějakém daném poměru stran. A aby toho nebylo málo, chcete, aby tento ořez nebyl definitivní. Uživatel bude moci v budoucnu přijít, a ořez nahraného obrázku upravit a přeuložit.

### Model obrázku

Začneme s modelem, tedy s _Doctrine Entitou_, která ponese informaci o obrázku a jeho ořezu. Jednoduchý může vypadat třeba takto:

    /**
     * @ORM\Entity
     */
    class Image {
    
        public function __construct($url = null) {
            $this->url = $url;
        }
    
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;
    
        /**
         * Relativní cesta k uloženému obrázku (ze složky 'web')
         * @ORM\Column(type="string")
         */
        private $url;
    
        /**
         * Požadovaný poměr stran ořezu
         * @ORM\Column(type="float", name="aspect_ratio")
         */
        private $aspectRatio;
    
        /**
         * X-souřadnice počátku ořezu - absolutní v pixelech
         * @ORM\Column(type="integer", name="start_x")
         */
        private $startX = 0;
    
        /**
         * Y-souřadnice počátku ořezu - absolutní v pixelech
         * @ORM\Column(type="integer", name="start_y")
         */
        private $startY = 0;
    
        /**
         * X-souřadnice konce ořezu - absolutní v pixelech
         * @ORM\Column(type="integer", name="end_x", nullable=TRUE)
         */
        private $endX;
    
        /**
         * Y-souřadnice počátku ořezu - absolutní v pixelech
         * @ORM\Column(type="integer", name="end_y", nullable=TRUE)
         */
        private $endY;


Do entity si také (kromě vygenerovaných getterů a setterů) můžete přidat pomocné metody, např. pro nastavování a resetování ořezu, získání reálných rozměrů či metadat uloženého obrázku atd. Při nahrání obrázku na server vytvoříte novou instanci této entity - zatím bez definovaného ořezu či s nějakým automaticky vypočítaným defaultním ořezem, a persistujete jí do databáze.

Ořezávání pak můžete na front-endu vyřešit nějakým java-scriptovým „ořezávátkem“ - např. jQuery plugin [Cropper](https://fengyuanchen.github.io/cropper/), který byl použit pro tvorbu dema. Důležité je hodnoty, které dostanete z takovéhoto nástroje, uložit do vašeho _Image_ objektu a persistovat do databáze.

### Vlastní filtr

V našem _Image_ objektu máme nyní uloženy koordináty klientem definovaného ořezu (v námi požadovaném poměru stran). Navíc budeme chtít oříznutý obrázek zvětšit/zmenšit na vhodnou velikost. Vytvoříme si tedy [vlastní filtr](http://symfony.com/doc/current/bundles/LiipImagineBundle/filters.html#load-your-custom-filters):

    namespace Vas\Namespace\Imagine\Filter\Loader;
    
    use Imagine\Filter\Basic\Crop;
    use Imagine\Filter\Basic\Resize;
    use Imagine\Image\Box;
    use Imagine\Image\Point;
    use Imagine\Image\ImageInterface;
    use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;
    class PersistentCropFilterLoader implements LoaderInterface{
        
        public function load (ImageInterface $image, array $options = array()) {
            $cropFilter = new Crop(
                new Point($options["start_x"], $options["start_y"]),
                new Box($options["crop_width"], $options["crop_height"])
            );
            $resizeFilter = new Resize(
                new Box($options["resize_width"], $options["resize_height"])
            );
            // nejprve crop, potom resize na velikost, kterou chci:
            $image = $cropFilter->apply($image);
            $image = $resizeFilter->apply($image);
            return $image;
        }
    }
    
Zde jsme jenom spojili předdefinované _Imagine_ filtry **Crop** a **Resize** a aplikovali jsme je po sobě na tentýž obrázek. Filtr ještě zaregistrujeme jako službu:
    
    # app/config/services.yml
    
    my_app.persistent_crop_filter:
        class: Vas\Namespace\Imagine\Filter\Loader\PersistentCropFilterLoader
        tags:
            - { name: liip_imagine.filter.loader, loader: persistent_crop_filter }
            
Nyní na něj můžeme odkázat v konfiguraci:
    
    # app/config/config.yml
    
    liip_imagine:
        resolvers:
               default:
                  web_path: ~
        filter_sets:
            cache: ~
            persistent_crop:
                filters:
                    persistent_crop_filter: { }
                    
Všimněte si prázdného pole, předaného našemu filter loaderu. To proto, že parametry (options) s informacemi o ořezu a velikosti chceme filtru předat až „za běhu“ z údajů, uložených v DB. Neznamená to však, že by se obrázek ořezával při každém požadavku znovu – jak jsem psal v úvodu, každý další požadavek se stejnými ořezovými údaji se již bere z cache. Pokud se ovšem údaje o ořezu/velikosti změní, obrázek se při dalším požadavku znovu prožene filtrem a opět se uloží do cache.

Nyní už bychom mohli skončit a volat náš vytvořený filtr z šablony - **image** je instance našeho _Image_ objektu, předaná do šablony z controlleru:

    {% set resize_width = 250 %}
    {% set runtimeConfig = {"persistent_crop_filter": {
        "start_x": image.startX,
        "start_y": => image.startY,
        "crop_width": => image.width,
        "crop_height": => image.height,
        "resize_width": resize_width,
        "resize_height":  resize_width / image.aspectRatio
    }} %}
    
    <img src="{{ image.url | imagine_filter('persistent_crop', runtimeConfig) }}">

### Twigové rozšíření

Vzhledem k tomu, že jediné dvě vstupní proměnné pro náš filtr jsou ve skutečnosti požadovaná šířka obrázku a náš _Image_ objekt (vše ostatní dopočítáme z nich), zdá se být výše popsané volání filtru v šabloně nepohodlné a kostrbaté. Vytvoříme si tedy vlastní rozšíření _Twigu_:

    namespace Vas\Namespace\Twig;
    use Vas\Namespace\Entity\Image;
    use Liip\ImagineBundle\Imagine\Cache\CacheManager;

    class CropFilterExtension extends \Twig_Extension {
    
        private $cacheManager;
    
        public function __construct(CacheManager $cacheManager) {
            $this->cacheManager = $cacheManager;
        }
    
        public function getFilters() {
            return array(
                new \Twig_SimpleFilter('crop_filter', array($this, 'cropFilter')),
            );
        }
    
        public function cropFilter(Image $image, $width) {
            // CROP:
            $startX = $image->getStartX();
            $startY = $image->getStartY();
            $crop_width = $image->getWidth();        // vraci dopocitanou sirku orezu
            $crop_height = $image->getHeight();     // vraci dopocitanou vysku orezu
            // RESIZE:
            $height = $width / $image->getAspectRatio();
            // OPTIONS:
            $options = array(
                "persistent_crop_filter" => array(
                    "start_x" => $startX,
                    "start_y" => $startY,
                    "crop_width" => $crop_width,
                    "crop_height" => $crop_height,
                    "resize_width" => $width,
                    "resize_height" => $height
                )
            );
            // ZAVOLAM VLASTNI LIIP_IMAGINE FILTR "persistent_crop":
            return new \Twig_Markup(
                $this->cacheManager->getBrowserPath($image->getUrl(), "persistent_crop", $options),
                'utf8'
            );
        }
    
        public function getName() {
            return 'crop_filter_extension';
        }
    }

a zaregistrujeme ho:

    # app/config/services.yml

    my_app.crop_filter_extension:
        class: Vas\Namespace\Twig\CropFilterExtension
        public: false
        arguments: ['@liip_imagine.cache.manager']
        tags:
            - { name: twig.extension }
        
Vykreslování obrázku se nám v šabloně podstatně zpřehlední:

    <img src="{{ asset(image | crop_filter("250")) }}">